"""Client for Open Chat Bot's"""

from .client import client
from .client_group import client_group
from .descriptor import descriptor
from .repository import repository
from .response import response
from .response_group import response_group

name = "openchatbotlient"
